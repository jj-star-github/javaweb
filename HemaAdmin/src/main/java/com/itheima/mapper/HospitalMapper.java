package com.itheima.mapper;

import com.itheima.domain.Dept;
import com.itheima.domain.Patient;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 22372
 */
public interface HospitalMapper {
    List<Dept> findAllDept();

    void savePatient(Patient patient);

    List<Patient> findAllPatient(@Param("name") String name);
}
